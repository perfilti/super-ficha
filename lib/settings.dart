import 'dart:io';
import 'package:http/http.dart' show get;
import 'models/user.model.dart';

const String DATABASE_NAME = "super_ficha.db";
const String TABLE_USUARIO = "usuarios";
const String TABLE_FICHATREINO = "fichatreino";

const String CREATE_USUARIOS_TABLE_SCRIPT =
    "CREATE TABLE usuarios(id TEXT PRIMARY KEY, bzid TEXT, nome TEXT, email TEXT, cpf TEXT, datanasc TEXT, telefone TEXT, cep TEXT, rua TEXT, numero TEXT, bairro TEXT, estado_id TEXT, cidade_id TEXT, tipo TEXT, image TEXT, validade_mensalidade TEXT, validade_ficha TEXT, academia TEXT, host TEXT)";

const String CREATE_FICHATREINO_TABLE_SCRIPT =
    "CREATE TABLE fichatreino(codigo INT PRIMARY KEY,codigo_dia TEXT,dia TEXT, exercicio TEXT, repeticao TEXT, checked TEXT)";

class Settings {
  static String apiUrl1 = "http://grupofrota.radicalfitness.com.br";
  static String apiUrl = "https://gerenciador.teknew.com.br";
  static String theme = "light";
  // ignore: deprecated_member_use
  static UserModel? user;
  static String tokenApp = "";
}

Future verificaNet() async {
  try {
    final result = await InternetAddress.lookup('google.com.br');
    if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
      return true;
    } else {
      return false;
    }
  } catch (e) {
    return false;
  }
}
