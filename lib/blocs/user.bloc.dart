import 'dart:convert';
import 'package:flutter/services.dart';
import 'package:super_ficha/models/academias.model.dart';
import 'package:super_ficha/models/authenticate-user.model.dart';
import 'package:super_ficha/models/ficha.treino.model.dart';
import 'package:super_ficha/models/user.model.dart';
import 'package:super_ficha/repositories/account.repository.dart';
import 'package:flutter/material.dart';
import 'package:super_ficha/repositories/ficha.treino.repository.dart';
import '../settings.dart';

class UserBloc extends ChangeNotifier {
  UserModel? user;

  List<AcademiasModel>? academias;

  List<UserModel?>? usuario;
  List<FichaTreinoModel?>? exercicios;
  List<FichaTreinoModel?>? exercicios_segunda;
  List<FichaTreinoModel?>? exercicios_terca;
  List<FichaTreinoModel?>? exercicios_quarta;
  List<FichaTreinoModel?>? exercicios_quinta;
  List<FichaTreinoModel?>? exercicios_sexta;
  List<FichaTreinoModel?>? exercicios_sabado;
  var retornoCartao;

  var repository = new AccountRepository();
  var repositoryFicha = new FichaTreinoRepository();

  UserBloc() {
    usuario = null;
    user = null;
    exercicios = null;
    exercicios_segunda = null;
    exercicios_terca = null;
    exercicios_quarta = null;
    exercicios_quinta = null;
    exercicios_sexta = null;
    exercicios_sabado = null;
    loadUser();
    loadFicha();
    //getCidadesDB();
  }

  Future<UserModel?> authenticate(cpf, academia, host) async {
    try {
      //var prefs = await SharedPreferences.getInstance();
      var res = await repository.authenticate(cpf, host);

      if (res.id != '0') {
        res.academia = academia;
        res.host = host;
        //repository.create(res);

        user = res;
        Settings.user = user!;
        notifyListeners();
      }
      //await prefs.setString('user', jsonEncode(res));

      return res;
    } catch (ex) {
      print(ex);
      user = null;
      return null;
    }
  }

  Future<FichaTreinoModel?> getFicha(usuario) async {
    repositoryFicha
        .getExercicios(usuario.host, usuario.bzid, usuario.id)
        .then((data) {
      data.forEach((element) {
        if (element.codigo! > 0) {
          repositoryFicha.create(element);
        }
      });
      loadFicha();
      notifyListeners();
      return 0;
    });
  }

  Future<FichaTreinoModel?> excluirFicha() async {
    repositoryFicha.deleteTable().then((data) {
      notifyListeners();
      return getFicha(user);
    });
  }

  Future<AcademiasModel?> getAcademias() async {
    try {
      //var prefs = await SharedPreferences.getInstance();
      var res = await repository.getAcademias();

      repository.getAcademias().then((data) {
        if (data.length > 0) {
          this.academias = data;
          notifyListeners();
        }
      });
    } catch (ex) {
      print(ex);
      user = null;
      return null;
    }
  }

  Future logout() async {
    user = null;
    Settings.user = null;
    repository.deleteBanco().then((value) {
      return 0;
    });
  }

  Future loadUser() async {
    repository.getUsuarios().then((data) {
      if (data.isNotEmpty) {
        usuario = data;
        Settings.user = data[0];
        user = data[0];
        notifyListeners();
      }
    });
  }

  Future loadFicha() async {
    repositoryFicha.getFichaTreino().then((data) {
      if (data.isNotEmpty) {
        exercicios = data;
        notifyListeners();
      }
    });
    repositoryFicha.getExercicioDia('1').then((data) {
      if (data.isNotEmpty) {
        exercicios_segunda = data;
        notifyListeners();
      }
    });
    repositoryFicha.getExercicioDia('2').then((data) {
      if (data.isNotEmpty) {
        exercicios_terca = data;
        notifyListeners();
      }
    });
    repositoryFicha.getExercicioDia('3').then((data) {
      if (data.isNotEmpty) {
        exercicios_quarta = data;
        notifyListeners();
      }
    });
    repositoryFicha.getExercicioDia('4').then((data) {
      if (data.isNotEmpty) {
        exercicios_quinta = data;
        notifyListeners();
      }
    });
    repositoryFicha.getExercicioDia('5').then((data) {
      if (data.isNotEmpty) {
        exercicios_sexta = data;
        notifyListeners();
      }
    });
    repositoryFicha.getExercicioDia('6').then((data) {
      if (data.isNotEmpty) {
        exercicios_sabado = data;
        notifyListeners();
      }
    });
  }
}
