class AuthenticateModel {
  String? cpf;
  String? token;

  AuthenticateModel({
    this.cpf,
    this.token,
  });

  AuthenticateModel.fromJson(Map<String, dynamic> json) {
    cpf = json['cpf'];
    token = json['token'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['cpf'] = this.cpf;
    data['token'] = this.token;
    return data;
  }
}
