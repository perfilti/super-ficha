class UserModel {
  String? id;
  String? bzid;
  String? nome;
  String? email;
  String? cpf;
  String? datanasc;
  String? telefone;
  String? cep;
  String? rua;
  String? numero;
  String? bairro;
  String? estado_id;
  String? cidade_id;
  String? tipo;
  String? image;
  String? validade_mensalidade;
  String? validade_ficha;
  String? academia;
  String? host;

  UserModel({
    this.id,
    this.bzid,
    this.nome,
    this.email,
    this.cpf,
    this.datanasc,
    this.telefone,
    this.cep,
    this.rua,
    this.numero,
    this.bairro,
    this.estado_id,
    this.cidade_id,
    this.tipo,
    this.image,
    this.validade_mensalidade,
    this.validade_ficha,
    this.academia,
    this.host,
  });

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'bzid': bzid,
      'nome': nome,
      'email': email,
      'cpf': cpf,
      'datanasc': datanasc,
      'telefone': telefone,
      'cep': cep,
      'rua': rua,
      'numero': numero,
      'bairro': bairro,
      'estado_id': estado_id,
      'cidade_id': cidade_id,
      'tipo': tipo,
      'image': image,
      'validade_mensalidade': validade_mensalidade,
      'validade_ficha': validade_ficha,
      'academia': academia,
      'host': host,
    };
  }

  UserModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    bzid = json['bzid'];
    nome = json['nome'];
    email = json['email'];
    cpf = json['cpf'];
    datanasc = json['datanasc'];
    telefone = json['telefone'];
    cep = json['cep'];
    rua = json['rua'];
    numero = json['numero'];
    bairro = json['bairro'];
    estado_id = json['estado_id'];
    cidade_id = json['cidade_id'];
    tipo = json['tipo'];
    image = json['image'];
    validade_mensalidade = json['validade_mensalidade'];
    validade_ficha = json['validade_ficha'];
    academia = json['academia'];
    host = json['host'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['bzid'] = this.bzid;
    data['nome'] = this.nome;
    data['email'] = this.email;
    data['cpf'] = this.cpf;
    data['datanasc'] = this.datanasc;
    data['telefone'] = this.telefone;
    data['cep'] = this.cep;
    data['rua'] = this.rua;
    data['numero'] = this.numero;
    data['bairro'] = this.bairro;
    data['estado_id'] = this.estado_id;
    data['cidade_id'] = this.cidade_id;
    data['tipo'] = this.tipo;
    data['image'] = this.image;
    data['validade_mensalidade'] = this.validade_mensalidade;
    data['validade_ficha'] = this.validade_ficha;
    data['academia'] = this.academia;
    data['host'] = this.host;
    return data;
  }
}
