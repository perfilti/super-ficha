class AcademiasModel {
  int? codigo;
  String? nome;
  String? host;

  AcademiasModel({this.codigo, this.nome, this.host});

  Map<String, dynamic> toMap() {
    return {
      'codigo': codigo,
      'nome': nome,
      'host': host,
    };
  }

  AcademiasModel.fromJson(Map<String, dynamic> json) {
    codigo = json['codigo'];
    nome = json['nome'];
    host = json['host'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['codigo'] = this.codigo;
    data['nome'] = this.nome;
    data['host'] = this.host;
    return data;
  }
}
