class FichaTreinoModel {
  int? codigo;
  String? codigo_dia;
  String? dia;
  String? exercicio;
  String? repeticao;
  String? checked = 'false';

  FichaTreinoModel(
      {this.codigo,
      this.codigo_dia,
      this.dia,
      this.exercicio,
      this.repeticao,
      this.checked = 'false'});

  Map<String, dynamic> toMap() {
    return {
      'codigo': codigo,
      'codigo_dia': codigo_dia,
      'dia': dia,
      'exercicio': exercicio,
      'repeticao': repeticao,
      'checked': checked,
    };
  }

  FichaTreinoModel.fromJson(Map<String, dynamic> json) {
    codigo = json['codigo'];
    codigo_dia = json['codigo_dia'];
    dia = json['dia'];
    exercicio = json['exercicio'];
    repeticao = json['repeticao'];
    checked = json['checked'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['codigo'] = this.codigo;
    data['codigo_dia'] = this.codigo_dia;
    data['dia'] = this.dia;
    data['exercicio'] = this.exercicio;
    data['repeticao'] = this.repeticao;
    data['checked'] = this.checked;
    return data;
  }
}
