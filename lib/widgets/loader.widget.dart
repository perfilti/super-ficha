import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:super_ficha/widgets/progress-indicator.widget.dart';

class Loader extends StatelessWidget {
  final object;
  final Function? callback;

  Loader({@required this.object, @required this.callback});

  @override
  Widget build(BuildContext context) {
    if (object == null)
      return Center(
        child: GenericProgressIndicator(),
      );

    if (object.length == 0) {
      return Column(
        children: <Widget>[
          SizedBox(
            height: 20,
          ),
          Text(
            "Nenhum item encontrado",
            style: TextStyle(
              fontSize: 24,
              fontFamily: 'FirasansCondensed',
            ),
            textAlign: TextAlign.center,
          ),
          Text(
            "Verifique sua conexão com a internet!",
            style: TextStyle(
              fontSize: 20,
              fontFamily: 'FirasansCondensed',
              color: Colors.red,
            ),
            textAlign: TextAlign.center,
          ),
        ],
      );
    }
    return callback!();
  }
}
