import 'dart:io';

import 'package:cpf_cnpj_validator/cpf_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:select_form_field/select_form_field.dart';
import 'package:super_ficha/blocs/user.bloc.dart';
import 'package:super_ficha/models/academias.model.dart';
import 'package:super_ficha/models/authenticate-user.model.dart';
import 'package:super_ficha/pages/home.dart';
import 'package:super_ficha/settings.dart';
import 'package:super_ficha/widgets/loader.widget.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final _formKey = GlobalKey<FormState>();

  final _scaffoldKey = GlobalKey<ScaffoldState>();

  var cpf = '';
  String _academia = "";
  String _host = "";
  var _init = 0;
  @override
  Widget build(BuildContext context) {
    var bloc = Provider.of<UserBloc>(context);
    final List<Map<String, dynamic>> _items = [];

    if (_init == 0) {
      bloc.academias = null;
      bloc.getAcademias();

      _init = 1;
    }

    return Scaffold(
      key: _scaffoldKey,
      body: ListView(
        children: [
          Container(
            height: 150,
            child: Image.asset(
              "assets/logo.png",
              fit: BoxFit.fitHeight,
            ),
          ),
          new Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SizedBox(
                  height: 50,
                ),
                Text(
                  "CONFIRA E ACOMPANHE SEUS EXERCICIOS,",
                  style: TextStyle(
                      color: Color(0xFF013469),
                      fontSize: 20,
                      fontFamily: 'PathwayGothicOne'),
                  textAlign: TextAlign.center,
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  "Na palma da sua mão!",
                  style: TextStyle(
                      color: Color(0xFF299fdd),
                      fontSize: 25,
                      fontFamily: 'Yesteryear'),
                  textAlign: TextAlign.center,
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  "INSIRA SEU CPF",
                  style: TextStyle(
                    color: Color(0xFF013469),
                    fontSize: 18,
                  ),
                  textAlign: TextAlign.center,
                ),
                Loader(
                  object: bloc.academias,
                  callback: () {
                    var lista = bloc.academias;
                    _items.clear();
                    var i = 0;
                    lista?.forEach((value) {
                      _items.add({'value': i, 'label': value.nome});
                      i = i + 1;
                    });
                    return Padding(
                      padding:
                          const EdgeInsets.only(top: 10, left: 50, right: 50),
                      child: Form(
                        key: _formKey,
                        child: Column(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.only(left: 10),
                              child: new TextFormField(
                                keyboardType: TextInputType.number,
                                maxLength: 11,
                                decoration: new InputDecoration(
                                  counterText: "",
                                  border: InputBorder.none,
                                  icon: FaIcon(FontAwesomeIcons.addressCard),
                                  hintText: "Somente Números",
                                ),
                                validator: (value) {
                                  cpf = CPFValidator.format(value!);

                                  if (!CPFValidator.isValid(cpf)) {
                                    return 'CPF Inválido';
                                  }
                                  return null;
                                },
                                onSaved: (val) {
                                  cpf = val!;
                                },
                              ),
                              decoration: new BoxDecoration(
                                color: Colors.white,
                                border: Border.all(
                                  color: Color(0XFF9c9c9c),
                                ),
                                borderRadius: BorderRadius.circular(5),
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Container(
                              decoration: BoxDecoration(
                                color: Colors.white,
                              ),
                              child: SelectFormField(
                                enableSearch: true,
                                type: SelectFormFieldType.dialog,
                                labelText: 'Academias',
                                textAlign: TextAlign.left,
                                style: TextStyle(color: Colors.black),
                                dialogTitle: 'Academias',
                                dialogCancelBtn: 'Fechar',
                                dialogSearchHint: 'Busca',
                                initialValue: '-1',
                                items: _items,
                                onChanged: (val) {
                                  setState(() {
                                    _academia =
                                        bloc.academias![int.parse(val)].nome!;
                                    _host =
                                        bloc.academias![int.parse(val)].host!;
                                  });
                                },
                              ),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Container(
                              width: 200,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(
                                Radius.circular(5),
                              )),
                              child: ElevatedButton(
                                child: new Text("ENTRAR"),
                                onPressed: () {
                                  if (_formKey.currentState!.validate()) {
                                    authenticate(bloc, context);
                                  }
                                },
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Future authenticate(bloc, BuildContext context) async {
    _onLoading(context);
    try {
      final result = await InternetAddress.lookup('google.com.br');
      verificaNet().then((resultado) async {
        if (resultado) {
          var user = await bloc.authenticate(
            cpf,
            _academia,
            _host,
          );

          if (user.id != '0') {
            await bloc.getFicha(user).then((data) {
              Navigator.pop(context);
              Navigator.pushReplacementNamed(context, '/home');
            });
          } else {
            _showDialog(context);
          }
        } else {
          Navigator.pop(context);
          EasyLoading.showToast(
            'Verifique sua conexão com a internet!',
          );
        }
      });
    } catch (e) {
      Navigator.pop(context);
      EasyLoading.showToast(
        'Verifique sua conexão com a internet!',
      );
    }
  }

  void _onLoading(BuildContext context) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Container(
          child: Center(
            child: CircularProgressIndicator(),
          ),
        );
      },
    );
  }

  void _showDialog(BuildContext context) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("CPF não cadastrado."),
          content: new Text("Você ainda não é aluno"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new ElevatedButton(
              child: new Text("FECHAR"),
              onPressed: () {
                Navigator.of(context).pop();
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
