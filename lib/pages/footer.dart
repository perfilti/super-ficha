import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:super_ficha/blocs/user.bloc.dart';

class Footer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var bloc = Provider.of<UserBloc>(context);

    return Container(
      padding: EdgeInsets.all(10),
      height: 60,
      color: Colors.white,
      child: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              width: 60,
              child: Column(
                children: [
                  GestureDetector(
                    child: Icon(FontAwesomeIcons.signOutAlt,
                        color: Color(0xFF013469)),
                    onTap: () {
                      bloc.logout().then((value) {
                        Navigator.pushReplacementNamed(context, '/login');
                      });
                    },
                  ),
                  Text('Sair',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Color(0xFF013469))),
                ],
              ),
            ),
            Container(
              width: 150,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  InkWell(
                    child: Icon(
                      FontAwesomeIcons.facebookSquare,
                      color: Color(0xFF013469),
                      size: 35,
                    ),
                  ),
                  InkWell(
                    child: Icon(
                      FontAwesomeIcons.instagram,
                      color: Color(0xFF013469),
                      size: 35,
                    ),
                  ),
                  InkWell(
                    child: Icon(
                      FontAwesomeIcons.youtube,
                      color: Color(0xFF013469),
                      size: 35,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
