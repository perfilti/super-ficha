import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:super_ficha/blocs/user.bloc.dart';

class Ficha extends StatefulWidget {
  int? dia = 0;
  Ficha({@required this.dia});
  @override
  _FichaState createState() => _FichaState();
}

class _FichaState extends State<Ficha> {
  @override
  Widget build(BuildContext context) {
    var bloc = Provider.of<UserBloc>(context);

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: DefaultTabController(
        initialIndex: calcularDiaSemana(widget.dia! - 1),
        length: 6,
        child: Scaffold(
          appBar: AppBar(
            bottom: TabBar(
              tabs: [
                Tab(text: "Seg"),
                Tab(text: "Ter"),
                Tab(text: "Qua"),
                Tab(text: "Qui"),
                Tab(text: "Sex"),
                Tab(text: "Sab"),
              ],
            ),
            title: Text(
              'Ficha Treino',
              textAlign: TextAlign.center,
            ),
          ),
          body: TabBarView(
            children: [
              bloc.exercicios_segunda != null
                  ? Tabela("Segunda Feira", bloc.exercicios_segunda!)
                  : nenhumRegistro(),
              bloc.exercicios_terca != null
                  ? Tabela("Terça Feira", bloc.exercicios_terca!)
                  : nenhumRegistro(),
              bloc.exercicios_quarta != null
                  ? Tabela("Quarta Feira", bloc.exercicios_quarta!)
                  : nenhumRegistro(),
              bloc.exercicios_quinta != null
                  ? Tabela("Quinta Feira", bloc.exercicios_quinta!)
                  : nenhumRegistro(),
              bloc.exercicios_sexta != null
                  ? Tabela("Sexta Feira", bloc.exercicios_sexta!)
                  : nenhumRegistro(),
              bloc.exercicios_sabado != null
                  ? Tabela("Sábado", bloc.exercicios_sabado!)
                  : nenhumRegistro(),
            ],
          ),
        ),
      ),
    );
  }
}

Widget Tabela(titulo, List exercicios) {
  return Column(
    children: <Widget>[
      Expanded(
        child: SingleChildScrollView(
          child: Column(children: [
            SizedBox(
              height: 15,
            ),
            Text(
              titulo,
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(
              height: 15,
            ),
            ConstrainedBox(
              constraints: BoxConstraints(minWidth: double.infinity),
              child: DataTable(
                columns: const <DataColumn>[
                  DataColumn(
                    label: Text(
                      'Exercício',
                      style: TextStyle(fontStyle: FontStyle.italic),
                    ),
                  ),
                  DataColumn(
                    label: Text(
                      'Repetição',
                      style: TextStyle(fontStyle: FontStyle.italic),
                    ),
                  ),
                ],
                rows: lista(exercicios),
              ),
            ),
          ]),
        ),
      ),
    ],
  );
}

calcularDiaSemana(dia) {
  if (dia == 6) {
    return 0;
  } else {
    return dia;
  }
}

nenhumRegistro() {
  return Column(
    children: [
      SizedBox(
        height: 10,
      ),
      Text(
        "Nenhum registro encontrado",
        style: TextStyle(fontWeight: FontWeight.bold),
        textAlign: TextAlign.center,
      ),
    ],
  );
}

lista(List exercicios) {
  return exercicios.map(
    ((element) {
      return DataRow(
        cells: <DataCell>[
          DataCell(Text(element.exercicio)),
          DataCell(Text(element.repeticao)),
        ],
      );
    }),
  ).toList();
}
