import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:super_ficha/blocs/user.bloc.dart';
import 'package:super_ficha/pages/home.dart';
import 'package:super_ficha/pages/login.dart';

class ValidarLoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var bloc = Provider.of<UserBloc>(context);

    return Scaffold(
      backgroundColor: Colors.white,
      body: bloc.user == null ? Login() : Home(atualizar: 1),
    );
  }
}
