import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:super_ficha/blocs/user.bloc.dart';
import 'package:super_ficha/pages/ficha.dart';
import 'package:super_ficha/pages/footer.dart';
import 'package:super_ficha/settings.dart';

class Home extends StatefulWidget {
  int? atualizar = 0;
  Home({@required this.atualizar});
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int _init = 0;
  @override
  Widget build(BuildContext context) {
    var bloc = Provider.of<UserBloc>(context);
    DateTime date = DateTime.now();

    atualizaFicha() {
      verificaNet().then((resultado) async {
        if (resultado) {
          await bloc.excluirFicha();
        }
      });
    }

    if (widget.atualizar == 1) {
      if (_init == 0) {
        atualizaFicha();
        _init = 1;
        widget.atualizar = 0;
      }
    }

    return Scaffold(
      backgroundColor: Color(0xFFb9d8e8),
      body: Column(
        children: <Widget>[
          Expanded(
            child: SingleChildScrollView(
                child: Column(
              children: <Widget>[
                SizedBox(
                  height: 30,
                ),
                Container(
                  height: 150,
                  child: Image.asset(
                    "assets/logo.png",
                    fit: BoxFit.fitHeight,
                  ),
                ),
                new Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      bloc.user == null
                          ? Text(
                              "Olá ",
                              style: TextStyle(
                                  color: Color(0xFF013469),
                                  fontSize: 20,
                                  fontFamily: 'PathwayGothicOne'),
                              textAlign: TextAlign.center,
                            )
                          : Text(
                              "Olá " +
                                  bloc.user!.nome!.split(" ")[0] +
                                  ", você esta na",
                              style: TextStyle(
                                  color: Color(0xFF013469),
                                  fontSize: 20,
                                  fontFamily: 'PathwayGothicOne'),
                              textAlign: TextAlign.center,
                            ),
                      Container(
                        margin: const EdgeInsets.only(right: 40, left: 40),
                        width: double.infinity,
                        decoration: BoxDecoration(
                          border: Border(
                            bottom: BorderSide(
                                width: 1.5, color: Color(0xFF000000)),
                          ),
                        ),
                        child: bloc.user == null
                            ? Text(
                                "",
                                style: TextStyle(
                                    color: Color(0xFF013469),
                                    fontSize: 50,
                                    fontWeight: FontWeight.bold,
                                    fontFamily: 'PathwayGothicOne'),
                                textAlign: TextAlign.center,
                              )
                            : Text(
                                "" + bloc.user!.academia!,
                                style: TextStyle(
                                    color: Color(0xFF013469),
                                    fontSize: 50,
                                    fontWeight: FontWeight.bold,
                                    fontFamily: 'PathwayGothicOne'),
                                textAlign: TextAlign.center,
                              ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(10),
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: <Widget>[
                              Container(
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(4),
                                ),
                                width: 150,
                                height: 120,
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(
                                        left: 8,
                                        right: 8,
                                        top: 8,
                                      ),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            "Ficha Treino",
                                            style: TextStyle(
                                                color: Colors.black45,
                                                fontSize: 20,
                                                fontFamily: 'PathwayGothicOne'),
                                          ),
                                          FaIcon(
                                            FontAwesomeIcons.clipboardList,
                                            color: Colors.black45,
                                          ),
                                        ],
                                      ),
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        bloc.user == null
                                            ? Text(
                                                "",
                                                style: TextStyle(
                                                    color: Color(0xFF90d5f9),
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 25,
                                                    fontFamily:
                                                        'PathwayGothicOne'),
                                              )
                                            : Text(
                                                "" + bloc.user!.validade_ficha!,
                                                style: TextStyle(
                                                    color: Color(0xFF90d5f9),
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 25,
                                                    fontFamily:
                                                        'PathwayGothicOne'),
                                              ),
                                      ],
                                    ),
                                    Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.end,
                                      children: [
                                        Container(
                                          width: 150,
                                          height: 20,
                                          decoration: BoxDecoration(
                                            color: Color(0xFF013469),
                                            borderRadius: BorderRadius.only(
                                              bottomLeft: Radius.circular(4),
                                              bottomRight: Radius.circular(4),
                                            ),
                                          ),
                                          child: Center(
                                            child: Text(
                                              "Validade",
                                              style: TextStyle(
                                                  color: Colors.white),
                                            ),
                                          ),
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                              ),
                              Container(
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(4),
                                ),
                                width: 150,
                                height: 120,
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(
                                        left: 8,
                                        right: 8,
                                        top: 8,
                                      ),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            "Mensalidade",
                                            style: TextStyle(
                                                color: Colors.black45,
                                                fontSize: 20,
                                                fontFamily: 'PathwayGothicOne'),
                                          ),
                                          FaIcon(
                                            FontAwesomeIcons.calendarAlt,
                                            color: Colors.black45,
                                          ),
                                        ],
                                      ),
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        bloc.user == null
                                            ? Text(
                                                "",
                                                style: TextStyle(
                                                    color: Color(0xFF90d5f9),
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 25,
                                                    fontFamily:
                                                        'PathwayGothicOne'),
                                              )
                                            : Text(
                                                "" +
                                                    bloc.user!
                                                        .validade_mensalidade!,
                                                style: TextStyle(
                                                    color: Color(0xFF90d5f9),
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 25,
                                                    fontFamily:
                                                        'PathwayGothicOne'),
                                              ),
                                      ],
                                    ),
                                    Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.end,
                                      children: [
                                        Container(
                                          width: 150,
                                          height: 20,
                                          decoration: BoxDecoration(
                                            color: Color(0xFF013469),
                                            borderRadius: BorderRadius.only(
                                              bottomLeft: Radius.circular(4),
                                              bottomRight: Radius.circular(4),
                                            ),
                                          ),
                                          child: Center(
                                            child: Text(
                                              "Vencimento",
                                              style: TextStyle(
                                                  color: Colors.white),
                                            ),
                                          ),
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                              ),
                            ]),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                          right: 10,
                          left: 10,
                          top: 40,
                        ),
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: <Widget>[
                              GestureDetector(
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  width: 350,
                                  height: 150,
                                  child: Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(
                                          left: 8,
                                          right: 8,
                                          top: 8,
                                        ),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Container(
                                              height: 40,
                                              child: Image.asset(
                                                "assets/icon_ficha.png",
                                                fit: BoxFit.fitHeight,
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Text(
                                            "Minha Ficha",
                                            style: TextStyle(
                                                color: Color(0xFF013469),
                                                fontWeight: FontWeight.bold,
                                                fontSize: 30,
                                                fontFamily: 'PathwayGothicOne'),
                                          ),
                                        ],
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          Container(
                                            height: 40,
                                            child: Image.asset(
                                              "assets/equalize.png",
                                              fit: BoxFit.fitHeight,
                                            ),
                                          )
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => Ficha(
                                              dia: date.weekday,
                                            )),
                                  );
                                },
                              ),
                            ]),
                      ),
                    ],
                  ),
                ),
              ],
            )),
          ),
          Footer(),
        ],
      ),
    );
  }

  void _AvisoSemFicha(BuildContext context) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Aviso!"),
          content: new Text("Você não possui uma ficha treino"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new ElevatedButton(
              child: new Text("FECHAR"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
