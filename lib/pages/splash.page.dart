import 'package:flutter/material.dart';
import 'package:super_ficha/pages/validar.login.page.dart';
import 'package:super_ficha/widgets/progress-indicator.widget.dart';

class SplashPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      // Replace the 3 second delay with your initialization code:
      future: Future.delayed(Duration(seconds: 3)),
      builder: (context, AsyncSnapshot snapshot) {
        // Show splash screen while waiting for app resources to load:
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Stack(
            children: <Widget>[
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    child: Center(
                      child: GenericProgressIndicator(),
                    ),
                  ),
                  Text("Aguarde"),
                ],
              ),
            ],
          );
        } else {
          // Loading is done, return the app:
          return ValidarLoginPage();
        }
      },
    );
  }
}
