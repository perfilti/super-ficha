import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:path/path.dart';
import 'package:super_ficha/models/ficha.treino.model.dart';
import 'package:super_ficha/settings.dart';
import 'package:sqflite/sqflite.dart';

class FichaTreinoRepository {
  Future<List<FichaTreinoModel>> getExercicios(
      host, String bzid, String id) async {
    var url = host + "/api/exercicios/" + bzid + "/" + id;
    Response response = await Dio().get(url);
    return (json.decode(response.data) as List)
        .map((course) => FichaTreinoModel.fromJson(course))
        .toList();
  }

  Future<Database> _getDatabase() async {
    return openDatabase(
      join(await getDatabasesPath(), DATABASE_NAME),
      onCreate: (db, version) {
        return db.execute(CREATE_FICHATREINO_TABLE_SCRIPT);
      },
      version: 3,
    );
  }

  Future create(FichaTreinoModel model) async {
    try {
      final Database db = await _getDatabase();
      db.transaction((txn) async {
        await txn.insert(TABLE_FICHATREINO, model.toMap(),
            conflictAlgorithm: ConflictAlgorithm.replace);
      });
      //print('salvo com sucesso');
    } catch (ex) {
      print(ex);
      return;
    }
  }

  Future deleteTable() async {
    try {
      final Database db = await _getDatabase();

      await db.delete(TABLE_FICHATREINO);
      return _getDatabase();
    } catch (ex) {
      print(ex);
      return;
    }
  }

  Future<List<FichaTreinoModel>> getFichaTreino() async {
    try {
      final Database db = await _getDatabase();
      final List<Map<String, dynamic>> maps = await db.query(TABLE_FICHATREINO);

      return List.generate(
        maps.length,
        (i) {
          return FichaTreinoModel(
            codigo: maps[i]['codigo'],
            codigo_dia: maps[i]['codigo_dia'],
            dia: maps[i]['dia'],
            exercicio: maps[i]['exercicio'],
            repeticao: maps[i]['repeticao'],
            checked: maps[i]['checked'],
          );
        },
      );
    } catch (ex) {
      print(ex);
      return <FichaTreinoModel>[];
    }
  }

  Future<List<FichaTreinoModel>> search(String term) async {
    try {
      final Database db = await _getDatabase();
      final List<Map<String, dynamic>> maps = await db.query(
        TABLE_FICHATREINO,
        where: "dia LIKE ?",
        whereArgs: [
          '%$term%',
        ],
      );

      return List.generate(
        maps.length,
        (i) {
          return FichaTreinoModel(
            codigo: maps[i]['codigo'],
            codigo_dia: maps[i]['codigo_dia'],
            dia: maps[i]['dia'],
            exercicio: maps[i]['exercicio'],
            repeticao: maps[i]['repeticao'],
            checked: maps[i]['checked'],
          );
        },
      );
    } catch (ex) {
      print(ex);
      return <FichaTreinoModel>[];
    }
  }

  Future<List<FichaTreinoModel>> getExercicioDia(String term) async {
    try {
      final Database db = await _getDatabase();
      final List<Map<String, dynamic>> maps = await db.query(
        TABLE_FICHATREINO,
        where: "codigo_dia LIKE ?",
        whereArgs: [
          '%$term%',
        ],
      );

      return List.generate(
        maps.length,
        (i) {
          return FichaTreinoModel(
            codigo: maps[i]['codigo'],
            codigo_dia: maps[i]['codigo_dia'],
            dia: maps[i]['dia'],
            exercicio: maps[i]['exercicio'],
            repeticao: maps[i]['repeticao'],
            checked: maps[i]['checked'],
          );
        },
      );
    } catch (ex) {
      print(ex);
      return <FichaTreinoModel>[];
    }
  }
}
