import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:super_ficha/models/academias.model.dart';
import 'package:super_ficha/models/authenticate-user.model.dart';
import 'package:sqflite/sqflite.dart';
import '../models/user.model.dart';
import 'package:path/path.dart';
import '../settings.dart';

class AccountRepository {
  Future<UserModel> authenticate(cpf, host) async {
    var url = host + "/api/login-ficha-treino-app";
    Response response =
        await Dio().post(url, data: {'cpf': cpf, 'token': 'AppSuperFicha'});

    return UserModel.fromJson(json.decode(response.data));
  }

  Future<List<AcademiasModel>> getAcademias() async {
    var url = "${Settings.apiUrl}/api/academias";
    Response response = await Dio().get(url);
    return (json.decode(response.data) as List)
        .map((course) => AcademiasModel.fromJson(course))
        .toList();
  }

  Future deleteBanco() async {
    String path = join(await getDatabasesPath(), DATABASE_NAME);
    // Delete the database
    await deleteDatabase(path).then((value) {
      return 0;
    });
  }

  Future<Database> _getDatabase() async {
    return openDatabase(
      join(await getDatabasesPath(), DATABASE_NAME),
      onCreate: (db, version) {
        db.execute(CREATE_FICHATREINO_TABLE_SCRIPT);
        return db.execute(CREATE_USUARIOS_TABLE_SCRIPT);
      },
      version: 3,
    );
  }

  Future updateImage(String id, String imagePath) async {
    try {
      final Database db = await _getDatabase();
      final model = await getUsuario(id);

      model.image = imagePath;

      await db.update(
        TABLE_USUARIO,
        model.toMap(),
        where: "id = ?",
        whereArgs: [model.id],
      );
    } catch (ex) {
      print(ex);
      return;
    }
  }

  Future create(UserModel model) async {
    try {
      final Database db = await _getDatabase();

      await db.insert(
        TABLE_USUARIO,
        model.toMap(),
        conflictAlgorithm: ConflictAlgorithm.replace,
      );
      //print('salvo com sucesso');
    } catch (ex) {
      print(ex);
      return;
    }
  }

  Future<UserModel> getUsuario(String id) async {
    try {
      final Database db = await _getDatabase();
      final List<Map<String, dynamic>> maps = await db.query(
        TABLE_USUARIO,
        where: "id = ?",
        whereArgs: [id],
      );

      return UserModel(
        id: maps[0]['id'],
        bzid: maps[0]['bzid'],
        nome: maps[0]['nome'],
        email: maps[0]['email'],
        cpf: maps[0]['cpf'],
        datanasc: maps[0]['datanasc'],
        telefone: maps[0]['telefone'],
        cep: maps[0]['cep'],
        rua: maps[0]['rua'],
        numero: maps[0]['numero'],
        bairro: maps[0]['bairro'],
        estado_id: maps[0]['estado_id'],
        cidade_id: maps[0]['cidade_id'],
        tipo: maps[0]['tipo'],
        image: maps[0]['image'],
        validade_mensalidade: maps[0]['validade_mensalidade'],
        validade_ficha: maps[0]['validade_ficha'],
        academia: maps[0]['academia'],
        host: maps[0]['host'],
      );
    } catch (ex) {
      print(ex);
      return new UserModel();
    }
  }

  Future<UserModel> getUsuarioCpf(String cpf) async {
    try {
      final Database db = await _getDatabase();
      final List<Map<String, dynamic>> maps = await db.query(
        TABLE_USUARIO,
        where: "cpf = ?",
        whereArgs: [cpf],
      );

      return UserModel(
        id: maps[0]['id'],
        bzid: maps[0]['bzid'],
        nome: maps[0]['nome'],
        email: maps[0]['email'],
        cpf: maps[0]['cpf'],
        datanasc: maps[0]['datanasc'],
        telefone: maps[0]['telefone'],
        cep: maps[0]['cep'],
        rua: maps[0]['rua'],
        numero: maps[0]['numero'],
        bairro: maps[0]['bairro'],
        estado_id: maps[0]['estado_id'],
        cidade_id: maps[0]['cidade_id'],
        tipo: maps[0]['tipo'],
        image: maps[0]['image'],
        validade_mensalidade: maps[0]['validade_mensalidade'],
        validade_ficha: maps[0]['validade_ficha'],
        academia: maps[0]['academia'],
        host: maps[0]['host'],
      );
    } catch (ex) {
      print(ex);
      return new UserModel();
    }
  }

  Future<List<UserModel>> getUsuarios() async {
    try {
      final Database db = await _getDatabase();
      final List<Map<String, dynamic>> maps = await db.query(TABLE_USUARIO);

      return List.generate(
        maps.length,
        (i) {
          return UserModel(
            id: maps[i]['id'],
            bzid: maps[i]['bzid'],
            nome: maps[i]['nome'],
            email: maps[i]['email'],
            cpf: maps[i]['cpf'],
            datanasc: maps[i]['datanasc'],
            telefone: maps[i]['telefone'],
            cep: maps[i]['cep'],
            rua: maps[i]['rua'],
            numero: maps[i]['numero'],
            bairro: maps[i]['bairro'],
            estado_id: maps[i]['estado_id'],
            cidade_id: maps[i]['cidade_id'],
            tipo: maps[i]['tipo'],
            image: maps[i]['image'],
            validade_mensalidade: maps[0]['validade_mensalidade'],
            validade_ficha: maps[0]['validade_ficha'],
            academia: maps[0]['academia'],
            host: maps[0]['host'],
          );
        },
      );
    } catch (ex) {
      print(ex);
      return <UserModel>[];
    }
  }
}
